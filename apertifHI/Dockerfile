FROM python:3.8-slim

ENV GO_VERSION=1.21.0
ENV SINGULARITY_VERSION=4.1.2

ARG APT_INSTALL="apt-get install --no-install-recommends -y"
ARG PIP_INSTALL="pip install --no-cache-dir"

RUN apt-get update
RUN apt-get dist-upgrade -y

ENV DEBIAN_FRONTEND=noninteractive
RUN ${APT_INSTALL} \
      wget curl acl sssd xterm software-properties-common less vim emacs git \
      python3-pip ipython3 build-essential snakemake montage \
      autoconf automake cryptsetup libfuse-dev libglib2.0-dev libseccomp-dev libtool \
      pkg-config runc squashfs-tools squashfs-tools-ng uidmap zlib1g-dev wcslib-dev imagemagick

RUN apt-get -y clean && \
    apt-get -y autoremove --purge --quiet

COPY SoFiA-image-pipeline .
RUN  pip install -r requirements.txt
RUN  python3 setup.py install

RUN mkdir /sofia2
ARG SOFIA_COMMIT=cbd8a7c1  # tagged as version 2.5.0
RUN git clone https://gitlab.com/SoFiA-Admin/SoFiA-2.git /sofia2
WORKDIR /sofia2
RUN git checkout ${SOFIA_COMMIT}
RUN chmod +x compile.sh
RUN ./compile.sh -fopenmp
RUN cp sofia /usr/local/bin

RUN ${PIP_INSTALL} \
    mosaic-queen \
    reproject \
    pandas \
    scikit-learn \
    tqdm

## Install singularity as per https://docs.sylabs.io/guides/main/user-guide/quick_start.html#quick-installation-steps
# Dependencies were already taken care of   
# Install go
ARG OS=linux
ARG ARCH=amd64
RUN wget https://dl.google.com/go/go$GO_VERSION.$OS-$ARCH.tar.gz && \
    tar -C /usr/local -xzvf go$GO_VERSION.$OS-$ARCH.tar.gz && \
    rm go$GO_VERSION.$OS-$ARCH.tar.gz

ENV PATH=${PATH}:/usr/local/go/bin

# Download Singularity from version 4.1.2, build and install
RUN wget https://github.com/sylabs/singularity/releases/download/v${SINGULARITY_VERSION}/singularity-ce-${SINGULARITY_VERSION}.tar.gz && \
    tar -xzf singularity-ce-${SINGULARITY_VERSION}.tar.gz && \
    rm singularity-ce-${SINGULARITY_VERSION}.tar.gz && \
    cd singularity-ce-${SINGULARITY_VERSION} && \
    ./mconfig && \
    make -C builddir && \
    make -C builddir install

RUN mkdir /code
RUN mkdir /data

WORKDIR /data

# add startup script
RUN mkdir /skaha
COPY startup.sh /skaha/
RUN chmod +x /skaha/startup.sh

# add /etc/nsswitch.conf
COPY nsswitch.conf /etc/

CMD ["/skaha/startup.sh"]


