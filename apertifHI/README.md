The Dockerfile here builds a container with several components of the apertif-pipeline
built in. Amongst other things it contains singularity, i.e. one can run
singuarlity/apptainer images from this image. In particular, this container was built to
call and run the apercal.sif singuarity image in apercal.
